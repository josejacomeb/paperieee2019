import numpy as np
import matplotlib.pyplot as plt

# data to plot
n_groups = 6
#commands= (24.05, 22.1, 22.05, 23.35 , 21.15,22.54)
#images=(471.1, 462.55, 447.95, 460.45, 440.05, 456.42)
commands= (68.15, 69.57, 132.05, 246.0 , 106, 124.35)
images=(257.82, 299.19, 220.98, 298.78, 223, 259)

# create plot
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.35
opacity = 0.8

rects1 = plt.bar(index, commands, bar_width,
alpha=opacity,
color='g',
label='Control commands')

rects2 = plt.bar(index + bar_width, images, bar_width,
alpha=opacity,
color='b',
label='Processed image')

plt.xlabel("Locations" , fontsize=15, fontweight='bold')
plt.ylabel('Time [ms]', fontsize=15, fontweight='bold')
plt.title('INTERNET LATENCY', fontsize=18, fontweight='bold')
plt.xticks(index + bar_width, ('Ambato', 'Pillaro', 'Puyo', 'Spain', 'USA', 'Average'), fontsize=12)
plt.setp(plt.gca().get_xticklabels(), rotation=30, horizontalalignment='right')

plt.legend()
plt.ylim((0,400))
# Turn on the grid
plt.minorticks_on()
plt.grid(which='major', linestyle='-', linewidth='0.1', color='red')
# Customize the minor grid
plt.grid(which='minor', linestyle=':', linewidth='0.1', color='black')

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.005   *height,
                '%.1f' % float(height), fontsize=10,
        ha='center', va='bottom')
autolabel(rects1)
autolabel(rects2)

plt.tight_layout()




plt.show()
