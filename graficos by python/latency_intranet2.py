import matplotlib.pyplot as plt
axisx=[1,2,3,4,5,6,7,8]
gears=[1,2,3,4,5,6]
latency_gears= [377.75 , 488.8 ,421.7 ,438.05, 406.15 ,418.49]
commads=[7,8]
latency_commads=[15.21,6.43]
label3 = ['Z=10', 'Z=20', 'Z=26', 'Z=32', 'Z=62', 'ALL GEAR','Input', 'Output']

fig, ax = plt.subplots()
rects1=ax.bar(gears,latency_gears, label="Processed image", color='b')
rects2=ax.bar(commads,latency_commads, label="Control commands", color='g')
label = ['Z=10', 'Z=20', 'Z=26', 'Z=32', 'Z=62', 'ALL GEAR','Input', 'Output']
plt.ylabel('Time [ms]', fontsize=12, fontweight='bold')
plt.xlabel("Package type" , fontsize=12, fontweight='bold')
plt.title('INTRANET LATENCY' , fontsize=15, fontweight='bold')
plt.xticks(axisx,label3)
#plt.xticks(commads, label2)

# Turn on the grid
plt.minorticks_on()
plt.grid(which='major', linestyle='-', linewidth='0.1', color='red')
# Customize the minor grid
plt.grid(which='minor', linestyle=':', linewidth='0.1', color='black')
plt.ylim((0,550))

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.005   *height,
                '%.1f' % float(height),
        ha='center', va='bottom')
autolabel(rects1)
autolabel(rects2)



plt.legend()

plt.show()
