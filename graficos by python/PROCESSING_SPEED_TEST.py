import numpy as np
import matplotlib.pyplot as plt

# data to plot
n_groups = 6
CPU= (24.05, 22.1, 22.05, 23.35 , 21.15,22.54)
Raspberry_pi=(471.1, 462.55, 447.95, 460.45, 440.05, 456.42)

# create plot
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.35
opacity = 0.8

rects1 = plt.bar(index, CPU, bar_width,
alpha=opacity,
color='g',
label='CPU')

rects2 = plt.bar(index + bar_width, Raspberry_pi, bar_width,
alpha=opacity,
color='b',
label='Raspberry Pi')

plt.xlabel("GEAR TYPE" , fontsize=14, fontweight='bold')
plt.ylabel('Time [ms]', fontsize=14, fontweight='bold')
plt.title('Processing speed test', fontsize=18, fontweight='bold')
plt.xticks(index + bar_width, ('Z=10', 'Z=20', 'Z=26', 'Z=32', 'Z=62', 'ALL GEAR'),fontsize=12)
plt.legend()
plt.ylim((0,600))
# Turn on the grid
plt.minorticks_on()
plt.grid(which='major', linestyle='-', linewidth='0.1', color='red')
# Customize the minor grid
plt.grid(which='minor', linestyle=':', linewidth='0.1', color='black')

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.005*height,
                '%.1f' % float(height),
        ha='center', va='bottom', fontsize=11)
autolabel(rects1)
autolabel(rects2)

plt.tight_layout()




plt.show()
