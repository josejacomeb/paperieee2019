import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt


objects = ('Z=10', 'Z=20', 'Z=26', 'Z=32', 'Z=62', 'ALL GEAR','Input', 'Output')
latency = [377.75 , 488.8 ,421.7 ,438.05, 406.15 ,418.49 ,60 ,90]
objects_pos = [i for i, _ in enumerate(objects)]

fig, ax = plt.subplots()
rects1 = ax.bar(objects_pos, latency, color=['blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'red', 'red'])

plt.xticks(objects_pos, objects)
plt.ylabel('TIME [ms]', fontsize=12, fontweight='bold')
plt.xlabel("PACKAGE TYPE" , fontsize=12, fontweight='bold')
plt.title('LATENCY INTRANET' , fontsize=14, fontweight='bold')
plt.rcParams["axes.labelweight"] = "bold"

# Turn on the grid
plt.minorticks_on()
plt.grid(which='major', linestyle='-', linewidth='0.1', color='red')
# Customize the minor grid
plt.grid(which='minor', linestyle=':', linewidth='0.1', color='black')
plt.ylim((0,550))
def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.005   *height,
                '%.1f' % float(height),
        ha='center', va='bottom')
autolabel(rects1)

plt.show()
