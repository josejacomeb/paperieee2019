x=[1 2 3 4 5 6 7];
y = [257.82; 299.19; 300; 290; 298.78; 314.76; 293.42];
h=bar(x,y);

set(h(1),"facecolor","blue")
%set(h(2),"facecolor","blue")
%text(1:length(y),y,num2str(y),'vert','bottom','horiz','center'); 
box("on")
 
%make the legend
legend("LATENCY","location","northwest")
h2=legend('right');
%remember to use the handler from
%the last time you call legend()
set(h2,"fontweight","bold")
 
%change axis properties
h=get (gcf, "currentaxes");
set(h,"fontweight","bold")
set(h,"xtick",[1 2 3 4 5 6 7])
set(h,"xticklabel",['AMBATO';'PILLARO';'QUITO';'GUAYAQUIL';'ESPA�A';'COLOMBIA'; 'AVERAGE'])

%set(h,"interpreter","tex")
xlabel("Sites");
ylabel("Time [ms]"); 
title("LATENCY INTERNET (CALL 100)");
 
 
print -dpng -color "-S800,800" latencyinternet.png