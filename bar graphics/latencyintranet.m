x=[1 2 3 4 5 6 7 8];
%y = [377.75 448.8 421.7 438.05 406.15 418.49 ; 50 80 ];
y = [ 377.75;448.8;  421.7; 438.05; 406.15; 418.49; 80 ; 60];
%y = [8.95 377.75; 7.6 448.8; 6.9 421.7; 7.35 438.05; 6.85 406.15; 7.53 418.49; 60  90];

h=bar(x,y);

set(h(1),"facecolor","red")
%set(h(2),"facecolor","blue")
%text(1:length(y),y,num2str(y),'vert','bottom','horiz','center'); 
box("on")
 
%make the legend
legend("GPU","RPI","location","northwest")
h2=legend('right');
%remember to use the handler from
%the last time you call legend()
set(h2,"fontweight","bold")
 
%change axis properties
h=get (gcf, "currentaxes");
set(h,"fontweight","bold")
set(h,"xtick",[1 2 3 4 5 6 7 8])
set(h,"xticklabel",['Z=10' ; 'Z=20' ;'Z=26'; 'Z=32' ;'Z=62' ;'ALL GEAR'; 'INPUT' ; 'OUTPUT'])
set(h,"xticklabel", ['Z=dffffdf10' ; 'Z=ff' ;'Z=26'; 'Z=ff32' ;'Z=fff' ;'ALL ffGEAR'; 'fff' ; 'OUffTPUT']);
%set(h,"interpreter","tex");
xlabel("GEAR COMMANDS");
xlabel("Gear Type");
ylabel("Time [ms]"); 
title("LATENCY INTRANET");
 
 
print -dpng -color "-S500,500" latencyintranet.png