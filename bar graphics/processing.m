x=[1 2 3 4 5 6];
y = [24.05 471.10; 22.10 462.55; 22.05 447.95; 23.35 460.45; 21.15 440.05; 22.54 456.42];
h=bar(x,y);

set(h(1),"facecolor","red")
set(h(2),"facecolor","green")
%text(1:length(y),y,num2str(y),'vert','bottom','horiz','center'); 
box("on")
 
%make the legend
legend("GPU","RPI","location","northwest")
h2=legend('right');
%remember to use the handler from
%the last time you call legend()
set(h2,"fontweight","bold")
 
%change axis properties
h=get (gcf, "currentaxes");
set(h,"fontweight","bold")
set(h,"xtick",[1 2 3 4 5 6])
set(h,"xticklabel",['Z=10';'Z=20';'Z=26';'Z=32';'Z=62';'ALL GEAR'])

%set(h,"interpreter","tex")
xlabel("Gear Type");
ylabel("Time [ms]"); 
title("PROCESSING SPEED TEST");
 
 
print -dpng -color "-S500,500" processing.png